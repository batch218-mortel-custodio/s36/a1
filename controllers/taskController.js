// this document contains our app feature in displaying and manipulating our databse


const Task = require("../models/task.js");

module.exports.getAllTask = () => {
	return Task.find({}).then(result => {
		return result;
	})
}


///////////////////////////////(2)


module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name : requestBody.name
	})

	return newTask.save().then((task, error) => {
		if(error){
			console.log(error);
			return false;
		}
		else{
			return task;
		}
	})
}



// "taskId" parameter will serve as storage of id in our url/link
module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err){
			console.log(err);
			return false;
		}
		else{
			console.log(`Successfully removed ${taskId}`);
			return removedTask;
		}

	})
}

module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, err) => {
		if(err){
			console.log(err);
			return false;
		}

		result.name = newContent.name;

		return result.save().then((updateTask, saveErr) =>{
			if(saveErr){
				console.log(saveErr);
				return false;
			}
			else{
				return updateTask;
			}

		})
		
	})
}



/*[GET - wildcard required]
1. Create a controller function for retrieving a specific task.
2. Create a route 
3. Return the result back to the client/Postman.
4. Process a GET request at the "/tasks/:id" route using postman to get a specific task.*/



module.exports.getTask = (taskId) => {
	return Task.findById(taskId).then((retrieveTask, err) => {
		if(err){
			console.log(err);
			return false;
		}
		else{
			console.log(`Successfully retrieved ${taskId}`);
			return retrieveTask;
		}

	})
}




/*[PUT - Update - wildcard Required]
5. Create a controller function for changing the status of a task to "complete".
6. Create a route
7. Return the result back to the client/Postman.
8. Process a PUT request at the "/tasks/:id/complete" route using postman to update a task.
*/


module.exports.updateStatus = (taskId, newContent) => {
	return Task.findById(taskId).then((result, err) => {
		if(err){
			console.log(err);
			return false;
		}

		result.status = newContent.status;

		return result.save().then((updateStatus, saveErr) =>{
			if(saveErr){
				console.log(saveErr);
				return false;
			}
			else{
				return updateStatus;
			}

		})
		
	})
}













/*
9. Create a git repository named S31.
10. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
11. Add the link in Boodle.*/
